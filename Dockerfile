FROM debian:stretch

WORKDIR /lib-maxiv-cppmascot
COPY . .

RUN apt-get update && apt-get install -y \
  build-essential autoconf git pkg-config \
  automake libtool curl make g++ unzip cmake wget vim \
  && apt-get clean

# install protobuf first, then grpc

ENV MY_INSTALL_DIR="/opt"
ENV PATH="/opt/bin:${PATH}"
RUN wget -q -O cmake-linux.sh https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0-Linux-x86_64.sh && sh cmake-linux.sh -- --skip-license --prefix=$MY_INSTALL_DIR && rm cmake-linux.sh
RUN git clone -b v1.28.0 https://github.com/grpc/grpc /grpc && \
    cd /grpc && git submodule update --init && mkdir -p cmake/build && cd cmake/build && \
    cmake -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF ../.. && \
    make && make install
