#ifndef DEVICE_PROXY_H
#define DEVICE_PROXY_H

#include "tango.h"
#include "grpcproto/device_server.grpc.pb.h"
#include "grpcproto/database.grpc.pb.h"
#include "grpcproto/attributes.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using mascot::AttributeConfig;
using mascot::AttributeConfigReply;
using mascot::DeviceAttribute;
using mascot::Directory;
using mascot::MascotDeviceServer;
using mascot::ReadAttributeMessage;
using mascot::ReadAttributeReply;
using std::string;
using std::vector;
//using mascot::AttributeInfoEx;
//using mascot::AttributeInfoList;
using mascot::DeviceName;
using mascot::DirectoryImportReply;
using mascot::WriteAttributeMessage;
using mascot::WriteReply;

namespace Tango
{
  class DeviceProxy : public Tango::Connection
  {
  private:
    //Tango::DbDevice *db_dev;
    std::string device_name;
    std::string alias_name;
    //DeviceInfo      info;
    bool get_address(const string &name, string &address);
    std::string name, server_address;

  public:
    DeviceProxy(std::string &name);
    //DeviceProxy(const char *name /*, grpc */);
    DeviceProxy(const DeviceProxy &);
    DeviceProxy &operator=(const DeviceProxy &);
    ~DeviceProxy();
    DeviceProxy(const char *name) : Connection() //db_dev(NULL)
    {
    }
    DeviceAttribute read_attribute(std::string &attr_name, DeviceAttribute &dev_attr);
    void write_attribute(DeviceAttribute &attr_in);

    /**
    * Ping a device.
    *
    * A method which sends a ping to the device and returns the time elapsed as microseconds. Example :
    * @code
    * cout << " device ping took " << my_device->ping() << " microseconds" << std::endl;
    * @endcode
    *
    * @throws ConnectionFailed, CommunicationFailed
    */
    virtual int ping();
  };

} // namespace Tango
#endif