#ifndef _APIUTIL_H
#define _APIUTIL_H

#include "tango.h"
#include "grpcproto/device_server.grpc.pb.h"
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
namespace Tango {
  /**
   * Miscellaneous utility methods useful in a Tango client.
   * 
   * This class is a singleton. It is not necessary to create it. It will be automatically done. 
   * A static method allows a user to retrieve the instance.
   * This is used by the DeviceProxy to get hold of the gRPC stub.
   * It also contains various convertion methods, to bridge the gap between gRPC related types and Tango types.
   */
class ApiUtil
{
public:
  ApiUtil(const ApiUtil&) = delete;
  void operator=(ApiUtil const&) = delete;
  /**
   * Returns the instance of this singleton class
   */
  static ApiUtil& instance();

  /**
   * corresponds to get_orb()
   */
  std::shared_ptr<mascot::MascotDeviceServer::Stub> create_and_get_stub(const std::string address);
  void set_stub(std::shared_ptr<mascot::MascotDeviceServer::Stub> stub_in);
  std::shared_ptr<mascot::MascotDeviceServer::Stub> get_stub();
  /**
   * Converts mascot::ReadAttributeReply to Tango::DeviceAttribute. 
   * The mascot::ReadAttributeReply contains a mascot::DeviceAttribute, which is what we want to parse.
   */
  void attr_to_device(mascot::ReadAttributeReply&&, DeviceAttribute&);

  /**
   * Converts a Tango::DeviceAttribute to a mascot::WriteAttributeMessage.
   */
  void device_to_wam(DeviceAttribute&&, mascot::WriteAttributeMessage&);



private:
  /**
   * Parses protobuf-generated class to Tango::AttrQuality
   */
  AttrQuality parseMascotQuality(mascot::AttrQuality);
  ApiUtil() {}
  std::shared_ptr<grpc::Channel> _channel;
  static ApiUtil _instance;
  std::shared_ptr<mascot::MascotDeviceServer::Stub> _stub;
  std::vector<std::string> host_ip_adrs;
  
};
}
#endif /* _APIUTIL_H */