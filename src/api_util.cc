#include "tango.h"

namespace Tango 
{


//should be addressed from inside 

ApiUtil& ApiUtil::instance() {
  return _instance;
}
std::shared_ptr<mascot::MascotDeviceServer::Stub> ApiUtil::create_and_get_stub(const std::string address)
{
  std::shared_ptr<grpc::Channel> channel(grpc::CreateChannel(address, grpc::InsecureChannelCredentials()));
  _stub = std::move(mascot::MascotDeviceServer::NewStub(channel));
  return _stub;
}

void ApiUtil::set_stub(std::shared_ptr<mascot::MascotDeviceServer::Stub> stub_in) { _stub = stub_in; }

std::shared_ptr<mascot::MascotDeviceServer::Stub> ApiUtil::get_stub() 
{ 
  return _stub; 
}

void ApiUtil::attr_to_device(mascot::ReadAttributeReply &&rar, DeviceAttribute &dev_attr)
{
  mascot::DeviceAttribute ar = std::move(rar.data());
  dev_attr.name = ar.name();
  dev_attr.quality = parseMascotQuality(ar.quality());

  switch(ar.value().value_case())
  {
    case mascot::AttributeValue::kBoolValue:
      dev_attr.boolValue = ar.value().boolvalue();
      break;
    case mascot::AttributeValue::kIntValue:
      dev_attr.intValue = ar.value().intvalue();
      break;
    case mascot::AttributeValue::kDoubleValue:
      dev_attr.doubleValue = ar.value().doublevalue();
      break;
    case mascot::AttributeValue::kStringValue:
      dev_attr.stringValue = ar.value().stringvalue();
      break;
  }
}

void ApiUtil::device_to_wam(DeviceAttribute&& dev_attr, mascot::WriteAttributeMessage& wam)
{
  wam.set_attributename(dev_attr.name);
  switch (dev_attr.valueType)
  {
  case Tango::ValueType::kIntValue:
    wam.mutable_value()->set_intvalue(dev_attr.intValue);
    break;
  case Tango::ValueType::kBoolValue:
    wam.mutable_value()->set_boolvalue(dev_attr.boolValue);
    break;
  case Tango::ValueType::kDoubleValue:
    wam.mutable_value()->set_doublevalue(dev_attr.doubleValue);
    break;
  case Tango::ValueType::kStringValue:
    wam.mutable_value()->set_stringvalue(dev_attr.stringValue);
    break;
  default:
    break;
  }
  

}

AttrQuality ApiUtil::parseMascotQuality(mascot::AttrQuality aq)
{
  switch (aq)
  {
  case mascot::ATTR_VALID:
    return ATTR_VALID;
    break;
  case mascot::ATTR_INVALID:
    return ATTR_INVALID;
    break;
  case mascot::ATTR_ALARM:
    return ATTR_ALARM;
    break;
  case mascot::ATTR_CHANGING:
    return ATTR_CHANGING;
    break;
  case mascot::ATTR_WARNING:
    return ATTR_WARNING;
    break;
  default:
    return ATTR_INVALID;
    break;
  }
}
} //namespace Tango

Tango::ApiUtil Tango::ApiUtil::_instance;