#ifndef _DEVAPI_H
#define _DEVAPI_H
#include "tango.h"
//namespace Tango 
//{
#include "DeviceData.h"
#include "DeviceAttribute.h"
#include "ApiUtil.h"
#include "Command.h"
#include "Connection.h"
#include "DeviceProxy.h"

//} // end of Tango namespace

#endif /* _DEVAPI_H */